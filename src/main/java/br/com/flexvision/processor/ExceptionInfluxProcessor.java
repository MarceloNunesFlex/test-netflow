package br.com.flexvision.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ExceptionInfluxProcessor implements Processor {
	private static final transient Logger LOG = LoggerFactory.getLogger(ExceptionInfluxProcessor.class);

	public void process(Exchange exchange) throws Exception {
		if(exchange.getIn().getHeader("X-Influxdb-Error")!=null)
			LOG.error("Erro interno do InfluxDB: "+exchange.getIn().getHeader("X-Influxdb-Error").toString());
	}

}
