package br.com.flexvision.processor;

import java.util.Random;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.ObjectMapper;
import br.com.flexvision.dto.Flow;

@Component
public class InfluxNative3 implements Processor {
	private static final transient Logger LOG = LoggerFactory.getLogger(InfluxNative3.class);

	public void process(Exchange exchange) throws Exception {
		String json = (String) exchange.getIn().getBody();
		LOG.debug(json);
		
		ObjectMapper mapper = new ObjectMapper();
		StringBuilder inserts = new StringBuilder("");
		
		for(String strFlow : json.split("\n")) {
			Flow flow = mapper.readValue(strFlow, Flow.class);
			
			inserts.append("flow")
			.append(",srcAddr=").append(flow.getSrcAddr())
			.append(",dstAddr=").append(flow.getDstAddr())
			.append(",prot=").append(flow.getProt())
			.append(" nextHop=\"").append(flow.getNextHop())
			.append("\",output=").append(flow.getOutput())
			.append(",srcPort=").append(flow.getSrcPort())
			.append(",dstPort=").append(flow.getDstPort())
			.append(",tos=").append(flow.getTos())
			.append(",tcpFlags=").append(flow.getTcpFlags())
			.append(",dstAs=").append(flow.getDstAs())
			.append(",dstMask=\"").append(flow.getDstMask())
			.append("\",version=").append(flow.getVersion())
			.append(",input=").append(flow.getInput())
			.append(",srcMask=").append(flow.getSrcMask())
			.append(",srcAs=").append(flow.getSrcAs())										
			.append(",dPkts=").append(flow.getdPkts())
			.append(",count=").append(flow.getCount())
			.append(",dOctets=").append(flow.getdOctets())
			.append(" ").append(flow.getEpochTime().toEpochMilli()).append("000000").append("\n");
		}

		exchange.getIn().removeHeaders("*");
		exchange.getIn().setHeader("CamelHttpMethod", "POST");
		exchange.getIn().setBody(inserts.toString());

		LOG.debug("Dados "+exchange.getIn().getBody().toString().substring(exchange.getIn().getBody().toString().length()-300));
	}
	
	public int randomInt(int valueMax){
		Random gerador = new Random();
        return gerador.nextInt(valueMax);
	}

}
